package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nha on 3/12/2015.
 */

@Entity
public class Warehouse extends  Model{
    @Id
    public Long id;

    public String name;

    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();  // trường quan hệ

    @OneToOne
    public Address address;

    public static Model.Finder<Long, Warehouse> find = new Model.Finder<>(Long.class, Warehouse.class);

    public static Warehouse findById(Long id) {
        return find.byId(id);
    }

    public String toString() {
        return name;
    }
}
