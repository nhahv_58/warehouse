package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Nha on 3/12/2015.
 */

@Entity // khang dịnh day là thực thể được ánh xạ tới cơ sở dữ liệu
public class Product extends Model implements PathBindable<Product> {

    @Id
    public Long id; // dinh danh khoa chinh

    @Constraints.Required
    public String ean;

    @Constraints.Required
    public String name;

    public String description;
// 8.3
//@Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;

    @OneToMany(mappedBy = "product")
    public List<StockItem> stockItems;

    public byte[] picture;

    // them vao 6.2.2
    @ManyToMany
    public List<Tag> tags ;
//    = new LinkedList<Tag>();




    // 7.4.3
    public static Finder<Long, Product> find = new Finder<Long, Product>(Long.class, Product.class);


    public Product() {
    }

    public Product(String ean, String name, String description) {
        this.ean = ean;
        this.name = name;
        this.description = description;
    }

    public String toString() {
        return String.format("%s - %s", ean, name);
    }


    private static List<Product> products;

    static {
        products = new ArrayList<Product>();
        products.add(new Product("1111111111111", "Paperclips 1",
                "Paperclips description 1"));
        products.add(new Product("2222222222222", "Paperclips 2",
                "Paperclips description 2"));
        products.add(new Product("3333333333333", "Paperclips 3",
                "Paperclips description 3"));
        products.add(new Product("4444444444444", "Paperclips 4",
                "Paperclips description 4"));
        products.add(new Product("5555555555555", "Paperclips 5",
                "Paperclips description 5"));
    }

    //    public static List<Product> findAll() {
//        return new ArrayList<Product>(products);
//    }
// 7.4.3
    public static List<Product> findAll() {
        return find.all();
    }

    //    public static Product findByEan(String ean) {
//        for (Product candidate : products) {
//            if (candidate.ean.equals(ean)) {
//                return candidate;
//            }
//        }
//        return null;
//    }
// 7.4.3
    public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }
//
//    public static List<Product> findByName(String term) {
//        final List<Product> results = new ArrayList<Product>();
//        for (Product candidate : products) {
//            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
//                results.add(candidate);
//            }
//        }
//        return results;
//    }

    public static List<Product> findByName(String term) {
        return find.where().eq("name", term).findList();
    }

    //7 .4.3
//
//    public static boolean remove(Product product) {
//        return products.remove(product);
//    }

    //    public void save() {
//        products.remove(findByEan(this.ean));
//        products.add(this);
//    }
// bind theo URL
    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }


    @Override
    public String javascriptUnbind() {
        return ean;
    }
// tim san pham
//    Product.find.byId(1L);

    public void delete() {
        for (Tag tag : tags) {
            tag.products.remove(this);
            tag.save();
        }
        super.delete();
    }

    // 7.5.4
    public static Page<Product> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(10)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
}
