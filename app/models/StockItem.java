package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by Nha on 3/12/2015.
 */

@Entity
public class StockItem extends Model {

    @Id
    public Long id;

    @ManyToOne
    public Warehouse warehouse;           // trường quan hệ nối với Warehouse

    @ManyToOne
    public Product product;               // trường quan hệ nối với Product

    public Long quantity;

    public static Model.Finder<Long, StockItem> find = new Model.Finder<>(Long.class, StockItem.class);


    public String toString() {
        return String.format("StockItem %d - %d x product %s",
                id, quantity, product == null ? null : product.id);
    }

    public static StockItem findById(Long id) {
        return find.byId(id);
    }
}
