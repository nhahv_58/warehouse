package controllers;

import models.StockItem;
import play.mvc.*;

import play.mvc.Result;

import java.util.List;

public class StockItems extends Controller {
    public static Result index() {
//        List<StockItem> items = StockItem.find.findList();
        List<StockItem> items = StockItem.find
                .where()
                .ge("quantity", 300) // so sanh >= 300
//                .orderBy("quantity")
                .orderBy("quantity desc") // sap xep giam dan
                .orderBy("quantity asc") // sap xep tang  dan
                .setMaxRows(5) // 5 item dau tien trong danh sach
                .findList();

        return ok(items.toString());
    }
}