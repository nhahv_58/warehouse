package controllers;

//import com.avaje.ebean.Ebean;
//import com.avaje.ebean.Page;
//import models.Product;
//import models.StockItem;
//import models.Tag;
//import play.data.Form;
//import play.mvc.Controller;
//import play.mvc.Result;
//import play.mvc.Security;
//import views.html.products.list;
//import views.html.products.details;
//
//import java.util.ArrayList;
//import java.util.List;

import com.avaje.ebean.Page;
import models.Product;
import models.StockItem;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.products.details;
import views.html.products.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nha on 3/12/2015.
 */

@Security.Authenticated(Secured.class)
public class Products extends Controller {

    // tu ham detail
    private static final Form<Product> productForm = Form.form(Product.class);

//7.5.4
//    public static Result list() {
//
//        List<Product> products = Product.findAll();
//        return ok(list.render(products));
//    }

    public static Result list(Integer page) {
        Page<Product> products = Product.find(page);
//        return ok(list.render(products));
        return ok(views.html.catalog.render(products));
    }

    public static Result newProduct() {

        return ok(details.render(productForm));
    }

    //        public static Result details(String ean) {
    public static Result details(Product product) {
// sau khi them product
//        final Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exist.", product.ean));//ean  product.ean
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }

    public static Result save() {

        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
        // 6.2.3
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags) {
            if (tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;


        //
//        7.3.3
//        product.save();
        if (product.id == null) {
            product.save();
//            Ebean.save(product);

        } else {
            product.update();
        }

        // quan he mot nhieu
        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
        stockItem.save();

        flash("success", String.format("Successfully added product %s", product));
        return redirect(routes.Products.list(0));
    }

    //    public static Result delete(String ean) {
//    public static Result delete(Product product) {
////        final Product product = Product.findByEan(ean);
//        if (product == null) {
//            return notFound(String.format("Product %s does not exists.", product.ean));//ean
//        }
//        Product.remove(product);
//        return redirect(routes.Products.list());
//    }

    // 7.4.3
//    public static Result delete(Product product) {
////        final Product product = Product.findByEan(ean);
//        Product product = Product.findByEan(ean);
//        if (product == null) {
//            return notFound(String.format("Product %s does not exists.", product.ean)); // ean
//        }
//// xoa ca stock item
//        for (StockItem stockItem : product.stockItems) {
//            stockItem.delete();
//        }
//
//        product.delete();
//        return redirect(routes.Products.list(0));
//    }

    public static Result delete(String ean) {
        Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
        product.delete();
        return redirect(routes.Products.list(0));
    }

}
